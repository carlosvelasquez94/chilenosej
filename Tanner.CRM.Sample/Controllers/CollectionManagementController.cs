﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Tanner.Automotive.CRM.Models;
using Tanner.Automotive.CRM.Services.Interfaces;
using Tanner.Utils.Extensions;

namespace Tanner.CRM.Sample.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CollectionManagementController : ControllerBase
    {
        private readonly ICollectionManagementService _service;

        private readonly IResponseManagementService _responseService;

        private readonly IClientService _clientService;

        public CollectionManagementController(ICollectionManagementService service, IResponseManagementService responseService, IClientService clientService)
        {
            _service = service;
            _responseService = responseService;
            _clientService = clientService;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            var date = DateTime.ParseExact("01-08-2019 13:32:47", "dd-MM-yyyy HH:mm:ss", new CultureInfo("es-CL"));
            CollectionManagement data1 = _service.FirstOrDefault(t => t.RUT == "12445624" && t.Operation == "144601" && t.Date == date);

            bool exist = _service.Any(t => t.RUT == "12445624" && t.Operation == "144601" && t.Date == date);


            ResponseManagement response = _responseService.FirstOrDefault(t => t.Code == "100");
            var rut = RUT.Parse("18431717-6");
            var intPart = rut.IntPart.ToString();
            Client client = _clientService.FirstOrDefault(t => t.RUT == intPart);
            //(IEnumerable<CollectionManagement>, int) data = _service.GetAllBy(t=> t.ID != null);
            var data = new CollectionManagement
            {
                Response= response,
                Committal = DateTime.ParseExact("29-07-2019", "dd-MM-yyyy", CultureInfo.InvariantCulture),
                Contact = 2,
                Date = DateTime.ParseExact("30-07-2019 10:13:15", "dd-MM-yyyy hh:mm:ss", CultureInfo.InvariantCulture),
                Email = "nelson.ortiz@tanner.cl",
                ExecutiveRUT = "184317176",
                Manager = new Manager {
                    Code = "130"
                },
                Observation = "observación de prueba",
                Operation = "158958",
                PhoneContact = "56934391128",
                RUT = client?.RUT ?? rut.IntPart.ToString(),
                Client = client,
                Name = client?.Name
            };
            //_service.UpSert(data.Item1.FirstOrDefault());
            _service.UpSert(data);
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

    }
}
