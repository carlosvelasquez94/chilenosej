# Tanner.Enterprise.CRM

## Release 1.0.2.0 - 2018-08-26
* Actualizando repositorios de CRM que permite la inserci�n de elementos y la b�squeda con expresiones lambda.

## Release 1.0.1.0 - 2018-08-08
* Agregando m�todo UpSert que permite la inserci�n o actualizaci�n de una entidad.

## Release 1.0.0.0 - 2019-06-28
* Versi�n inicial