﻿

using System;
using Tanner.CRM.Models;
using Tanner.CRM.Utils;

namespace Tanner.Enterprise.CRM.Models
{
    [CRMName("tann_documento")]
    public class CollectionDocument : IEntityBase
    {

        [CRMName("tann_documentoid", IsKey = true)]
        public Guid ID { get; set; }

        /// <summary>
        /// Identificador del documento en el Core
        /// </summary>
        [CRMName("tann_iddocumento")]
        public string DocumentID { get; set; }

        [CRMName("tann_numerodocumento")]
        public string DocumentNumber { get; set; }
    }
}
