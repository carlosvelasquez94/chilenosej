﻿
using System;
using Tanner.CRM.Models;
using Tanner.CRM.Utils;

namespace Tanner.Enterprise.CRM.Models
{
    [CRMName("tann_loggestiondecobranza")]
    public class CollectionManagement : IEntityBase
    {
        [CRMName("tann_loggestiondecobranzaid", IsKey = true)]
        public Guid ID { get; set; }

        /// <summary>
        /// Observaciones
        /// </summary>
        [CRMName("tann_observacion")]
        public string Comment { get; set; }

        /// <summary>
        /// Motivo
        /// </summary>
        [CRMName("statuscode")]
        public int Motive { get; set; }

        /// <summary>
        /// Condición para el pago
        /// </summary>
        public string Condition { get; set; }
        
        /// <summary>
        /// Fecha última gestión
        /// </summary>
        [CRMName("tann_fechadegestion")]
        public DateTime? LastManagementDate { get; set; }

        /// <summary>
        /// Próxima acción
        /// </summary>
        [CRMName("tann_proximaaccion")]
        public string NextAction { get; set; }

        /// <summary>
        /// Fecha próxima acción
        /// </summary>
        [CRMName("tann_fechaproximaaccion")]
        public DateTime? NextActionDate { get; set; }

        /// <summary>
        /// Fecha de pago tentativa
        /// </summary>
        public DateTime? TentativePayDate { get; set; }

        /// <summary>
        /// Documento de cobranza relacionado
        /// </summary>
        [CRMName("tann_iddocumento")]
        public CollectionDocument CollectionDocument { get; set; }
    }
}
