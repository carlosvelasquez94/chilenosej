﻿
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tanner.Enterprise.CRM.Models;
using Tanner.CRM.Repositories;
using Tanner.CRM.Services.Interfaces;
using Tanner.CRM.Services;
using Tanner.Enterprise.CRM.Services.Interfaces;

namespace Tanner.Enterprise.CRM.Services
{
    public class CollectionManagementService: ServiceBase<CollectionManagement>, ICollectionManagementService
    {        
        public CollectionManagementService(IRepositoryBase<CollectionManagement> repository) : base(repository)
        {
           
        }

        public IEnumerable<CollectionManagement> GetCollectionManagementByDocID(string documentID)
        {
            IEnumerable<CollectionManagement> result = _repository.GetAllBy(t => t.CollectionDocument.DocumentID == documentID);
            return result;
        }
    }
}
