﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tanner.CRM.Services.Interfaces;
using Tanner.Enterprise.CRM.Models;

namespace Tanner.Enterprise.CRM.Services.Interfaces
{
    public interface ICollectionManagementService : IServiceBase<CollectionManagement>
    {
        IEnumerable<CollectionManagement> GetCollectionManagementByDocID(string documentID);
    }
}
