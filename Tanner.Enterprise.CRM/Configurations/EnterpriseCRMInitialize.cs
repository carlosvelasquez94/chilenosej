﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Tanner.CRM.Configurations;
using Tanner.Enterprise.CRM.Services;
using Tanner.Enterprise.CRM.Services.Interfaces;

namespace Tanner.Enterprise.CRM.Configurations
{
    public static class EnterpriseCRMInitialize
    {
        public static void AddTannerEnterpriseCRM(this IServiceCollection services, Action<CRMConfiguration> option)
        {
            services.AddTannerCRM(option);
            Common(services);
        }

        public static void AddTannerEnterpriseCRM(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTannerCRM(configuration);
            Common(services);
        }

        private static void Common(IServiceCollection services)
        {
            services.AddScoped<ICollectionManagementService, CollectionManagementService>();
        }
    }
}
