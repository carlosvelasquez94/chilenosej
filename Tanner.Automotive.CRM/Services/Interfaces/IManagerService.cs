﻿
using Tanner.Automotive.CRM.Models;
using Tanner.CRM.Services.Interfaces;

namespace Tanner.Automotive.CRM.Services.Interfaces
{
    public interface IManagerService : IServiceBase<Manager>
    {
    }
}