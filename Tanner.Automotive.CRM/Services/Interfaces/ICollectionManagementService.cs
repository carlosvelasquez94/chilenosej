﻿using Tanner.CRM.Services.Interfaces;
using Tanner.Automotive.CRM.Models;

namespace Tanner.Automotive.CRM.Services.Interfaces
{
    public interface ICollectionManagementService : IServiceBase<CollectionManagement>
    {
    }
}
