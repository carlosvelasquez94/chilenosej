﻿using Tanner.Automotive.CRM.Models;
using Tanner.Automotive.CRM.Services.Interfaces;
using Tanner.CRM.Repositories;
using Tanner.CRM.Services;

namespace Tanner.Automotive.CRM.Services
{
    public class ManagerService : ServiceBase<Manager>, IManagerService
    {
        public ManagerService(IRepositoryBase<Manager> repository) : base(repository)
        {
        }
    }
}
