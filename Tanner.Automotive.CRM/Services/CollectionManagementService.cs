﻿using Tanner.Automotive.CRM.Models;
using Tanner.CRM.Repositories;
using Tanner.CRM.Services;
using Tanner.Automotive.CRM.Services.Interfaces;

namespace Tanner.Automotive.CRM.Services
{
    public class CollectionManagementService: ServiceBase<CollectionManagement>, ICollectionManagementService
    {        
        public CollectionManagementService(IRepositoryBase<CollectionManagement> repository) : base(repository)
        {
           
        }
    }
}
