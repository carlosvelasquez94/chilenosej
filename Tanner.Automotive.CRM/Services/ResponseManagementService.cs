﻿
using Tanner.Automotive.CRM.Models;
using Tanner.Automotive.CRM.Services.Interfaces;
using Tanner.CRM.Repositories;
using Tanner.CRM.Services;

namespace Tanner.Automotive.CRM.Services
{
    public class ResponseManagementService : ServiceBase<ResponseManagement>, IResponseManagementService
    {
        public ResponseManagementService(IRepositoryBase<ResponseManagement> repository) : base(repository)
        {

        }
    }
}
