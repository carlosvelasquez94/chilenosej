﻿
using Tanner.Automotive.CRM.Models;
using Tanner.Automotive.CRM.Services.Interfaces;
using Tanner.CRM.Repositories;
using Tanner.CRM.Services;

namespace Tanner.Automotive.CRM.Services
{
    public class ClientService: ServiceBase<Client>, IClientService
    {
        public ClientService(IRepositoryBase<Client> repository) : base(repository)
        {

        }
    }
}
