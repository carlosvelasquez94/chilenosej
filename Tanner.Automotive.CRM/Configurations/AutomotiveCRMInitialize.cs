﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Tanner.Automotive.CRM.Services;
using Tanner.Automotive.CRM.Services.Interfaces;
using Tanner.CRM.Configurations;

namespace Tanner.Automotive.CRM.Configurations
{
    public static class AutomotiveCRMInitialize
    {
        public static void AddTannerAutomotiveCRM(this IServiceCollection services, Action<CRMConfiguration> option)
        {
            services.AddTannerCRM(option);
            Common(services);
        }

        public static void AddTannerAutomotiveCRM(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTannerCRM(configuration);
            Common(services);
        }

        private static void Common(IServiceCollection services)
        {
            services.AddScoped<ICollectionManagementService, CollectionManagementService>();
            services.AddScoped<IResponseManagementService, ResponseManagementService>();
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IManagerService, ManagerService>();
        }
    }
}
