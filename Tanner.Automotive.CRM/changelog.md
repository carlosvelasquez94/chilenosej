# Tanner.Automotive.CRM

## Release 1.0.4.3 - 2019-09-02
* Agregando entidad Gestor "Manager" con su servicio.

## Release 1.0.4.2 - 2019-08-26
* Actualizando paquete Taner.CRM que permite la búsqueda con expresiones lambda.

## Release 1.0.4.1 - 2019-08-23
* Actualizando paquete Taner.CRM con la funcionalidad Any en el servicio base para saber si un elemento de una entidad en CRM existe.

## Release 1.0.4.0 - 2019-08-22
* Permitiendo que la fecha de compromiso para la gestión de cobranza sea nula.

## Release 1.0.3.0 - 2019-08-20
* Agregando propiedad para digito verificador del rut en la entidad de gestión de cobranza.

## Release 1.0.2.0 - 2019-08-19
* Agregando servicios para las entidades Client y ResponseManagement
* Actualizando datos de la entidad CollectionManagement

## Release 1.0.1.0 - 2019-08-08
* Agregando método UpSert que permite la inserción o actualización de una entidad.

## Release 1.0.0.0 - 2019-06-28
* Versión inicial