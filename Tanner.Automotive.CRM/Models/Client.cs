﻿
using System;
using Tanner.CRM.Models;
using Tanner.CRM.Utils;

namespace Tanner.Automotive.CRM.Models
{
    [CRMName("account")]
    public class Client : IEntityBase
    {

        [CRMName("accountid", IsKey = true)]
        public Guid ID { get; set; }
        
        [CRMName("accountnumber")]
        public string RUT { get; set; }

        [CRMName("wit_nombres")]
        public string Name { get; set; }
    }
}
