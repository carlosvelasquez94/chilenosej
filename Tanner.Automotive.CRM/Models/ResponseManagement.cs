﻿
using System;
using Tanner.CRM.Models;
using Tanner.CRM.Utils;

namespace Tanner.Automotive.CRM.Models
{
    /// <summary>
    /// Entity for CRM Automotive response management
    /// </summary>
    [CRMName("tann_respuestagestion")]
    public class ResponseManagement : IEntityBase
    {

        [CRMName("tann_respuestagestionid", IsKey = true)]
        public Guid ID { get; set; }

        [CRMName("tann_codigo")]
        public string Code { get; set; }
    }
}
