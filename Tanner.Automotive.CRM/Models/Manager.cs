﻿using System;
using Tanner.CRM.Models;
using Tanner.CRM.Utils;

namespace Tanner.Automotive.CRM.Models
{
    /// <summary>
    /// Entity for CRM Automotive manager
    /// </summary>
    [CRMName("tann_gestor")]
    public class Manager : IEntityBase
    {
        [CRMName("tann_gestorid", IsKey = true)]
        public Guid ID { get; set; }

        [CRMName("tann_name")]
        public string Name { get; set; }

        [CRMName("tann_codigo")]
        public string Code { get; set; }
    }
}
