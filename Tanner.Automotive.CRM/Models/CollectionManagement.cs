﻿
using Microsoft.Xrm.Sdk;
using System;
using Tanner.CRM.Models;
using Tanner.CRM.Utils;

namespace Tanner.Automotive.CRM.Models
{
    /// <summary>
    /// Entity for CRM Automotive Collection management
    /// </summary>
    [CRMName("tann_gestioncobranzaautomotriz")]
    public class CollectionManagement : IEntityBase
    {
        /// <summary>
        /// ID
        /// </summary>
        [CRMName("tann_gestioncobranzaautomotrizid", IsKey = true)]
        public Guid ID { get; set; }

        /// <summary>
        /// Client name
        /// </summary>
        [CRMName("tann_name")]
        public string Name { get; set; }

        /// <summary>
        /// Client RUT (Enterprise o Person)
        /// </summary>
        [CRMName("tann_rutcliente")]
        public string RUT { get; set; }

        /// <summary>
        /// Client
        /// </summary>
        [CRMName("tann_cliente")]
        public Client Client { get; set; }

        /// <summary>
        /// Operation number collection
        /// </summary>
        [CRMName("tann_operacion")]
        public string Operation { get; set; }
        
        /// <summary>
        /// Manager code 
        /// </summary>
        [CRMName("tann_gestor")]
        public Manager Manager { get; set; }

        /// <summary>
        /// Date of Committal
        /// </summary>
        [CRMName("tann_compromiso")]
        public DateTime? Committal { get; set; }

        /// <summary>
        /// Response management
        /// </summary>
        [CRMName("tann_respuesta")]
        public ResponseManagement Response { get; set; }

        /// <summary>
        /// Management gloss
        /// </summary>
        [CRMName("tann_observacion")]
        public string Observation { get; set; }

        /// <summary>
        /// Code of contact
        /// </summary>
        [CRMName("tann_mediocontacto", TypeInCRM = typeof(OptionSetValue))]
        public int Contact { get; set; }

        /// <summary>
        /// Date managers
        /// </summary>
        [CRMName("tann_fechayhoragestion")]
        public DateTime Date { get; set; }

        /// <summary>
        /// Executive rut
        /// </summary>
        [CRMName("tann_rutejecutivo")]
        public string ExecutiveRUT { get; set; }

        /// <summary>
        /// Phone for management
        /// </summary>
        [CRMName("tann_contactoutilizado")]
        public string PhoneContact { get; set; }

        /// <summary>
        /// Email used
        /// </summary>
        [CRMName("tann_correoutilizado", TypeInCRM = typeof(EntityReference))]
        public string Email { get; set; }

        /// <summary>
        /// Rut check digit
        /// </summary>
        [CRMName("tann_dv")]
        public string RUTDV { get; set; }
    }
}
