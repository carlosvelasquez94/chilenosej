﻿using System;

namespace Tanner.CRM.Utils
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property, AllowMultiple = false)]
    public class CRMNameAttribute: Attribute
    {
        public string Name { get; set; }

        public bool IsKey { get; set; }

        public Type TypeInCRM { get; set; }

        public CRMNameAttribute(string name)
        {
            Name = name;
        }
    }
}
