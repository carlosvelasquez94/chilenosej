﻿
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Tanner.CRM.Models;

namespace Tanner.CRM.Utils
{
    public static class CRMExtensions
    {
        public static (string name, string key) GetCRMData<T>() where T: IEntityBase
        {
            Type type = typeof(T);
            return GetCRMData(type);
        }
        private static (string name, string key) GetCRMData(Type type)
        {
            CRMNameAttribute attr = type.GetCustomAttributes(typeof(CRMNameAttribute), false).OfType<CRMNameAttribute>().FirstOrDefault();
            string name = type.Name;
            if (attr != null)
            {
                name = attr.Name;
            }
            string key = GetKey(type);
            (string name, string key) result = (name, key);
            return result;
        }

        private static string GetKey(Type type)
        {
            PropertyInfo[] properties = type.GetProperties();
            CRMNameAttribute attr = properties
                .SelectMany(t=> t.GetCustomAttributes(typeof(CRMNameAttribute), false).OfType<CRMNameAttribute>().Where(tt=> tt.IsKey))
                .FirstOrDefault();
            string result = null;
            if (attr != null)
            {
                result = attr.Name;
            }
            return result;
        }

        public static string GetName<T>(this string property) where T : IEntityBase
        {
            return GetName(property, typeof(T));
        }
        private static string GetName(this string property, Type type)
        {
            PropertyInfo prop = type.GetProperty(property);
            CRMNameAttribute attr = prop?.GetCustomAttributes(typeof(CRMNameAttribute), false).OfType<CRMNameAttribute>().FirstOrDefault();
            string result = null;
            if (attr != null)
            {
                result = attr.Name;
            }
            return result;
        }

        private static CRMNameAttribute GetCRMAttr(this string property, Type type)
        {
            PropertyInfo prop = type.GetProperty(property);
            CRMNameAttribute result = prop?.GetCustomAttributes(typeof(CRMNameAttribute), false).OfType<CRMNameAttribute>().FirstOrDefault();
            return result;
        }

        public static T ToEntityBase<T>(this Entity entity) where T: IEntityBase
        {
            Type typeT = typeof(T);
            T result = (T)Activator.CreateInstance(typeT);

            PropertyInfo[] properties = typeT.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                string name = property.GetCustomAttribute<CRMNameAttribute>()?.Name;                

                if (!string.IsNullOrEmpty(name))
                {
                    try

                    {
                        if (property.PropertyType.GetInterfaces().Contains(typeof(IEntityBase)))
                        {
                            object value = entity.ToEntityBase(property.PropertyType);
                            property.SetValue(result, value);
                        }
                        else
                        {
                            object value = entity.Attributes[name];

                            if (value is OptionSetValue optionSetVal)
                            {
                                property.SetValue(result, optionSetVal.Value);
                            }
                            else if (value is EntityReference entityRef)
                            {

                            }
                            else if (value is AliasedValue aliased)
                            {
                                property.SetValue(result, aliased.Value);
                            }
                            else
                            {
                                property.SetValue(result, value);
                            }
                        }                        
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            return result;
        }

        private static object ToEntityBase(this Entity entity, Type type)
        {
            (string nameEntity, _) = GetCRMData(type);

            object result = Activator.CreateInstance(type);
            var properties = type.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                var name = property.GetCustomAttribute<CRMNameAttribute>()?.Name;

                if (!string.IsNullOrEmpty(name))
                {
                    try
                    {
                        var tempName = $"{nameEntity}1.{name}";
                        if (!property.PropertyType.GetInterfaces().Contains(typeof(IEntityBase)))
                        {
                            object value = entity.Attributes[tempName];

                            if (value is OptionSetValue optionSetVal)
                            {
                                property.SetValue(result, optionSetVal.Value);
                            }
                            else if (value is EntityReference entityRef)
                            {

                            }
                            else if (value is AliasedValue aliased)
                            {
                                property.SetValue(result, aliased.Value);
                            }
                            else
                            {
                                property.SetValue(result, value);
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            return result;
        }

        public static Entity ToEntity<T>(this T entity) where T : IEntityBase
        {
            Type typeT = typeof(T);
            string nameT = typeT.GetCustomAttribute<CRMNameAttribute>()?.Name;

            var result = new Entity(nameT, entity.ID)
            {
                Attributes = new AttributeCollection()
            };            
            
            var properties = typeT.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                CRMNameAttribute att = property.GetCustomAttribute<CRMNameAttribute>();

                if (!string.IsNullOrEmpty(att?.Name))
                {
                    try
                    {
                        object propertyValue = property.GetValue(entity);
                        if (propertyValue is IEntityBase propertyEntity)
                        {
                            Type typeEntity = propertyValue.GetType();
                            string entityName = typeEntity.GetCustomAttribute<CRMNameAttribute>()?.Name;
                            propertyValue = new EntityReference(entityName, propertyEntity.ID);
                        }
                        else if (att.TypeInCRM == typeof(OptionSetValue) && propertyValue is int valueInt)
                        {
                            propertyValue = new OptionSetValue(valueInt);
                        }
                        result.Attributes.Add(att.Name, propertyValue);
                    }
                    catch (Exception)
                    {
                    }

                }
            }
            return result;
        }

        public static QueryExpression ToExpression<T>(this Expression<Func<T, bool>> predicate) where T: IEntityBase
        {
            QueryExpression result = null;

            if (predicate != null)
            {
                if (predicate.Body is BinaryExpression expression)
                {
                    result = new QueryExpression {
                        EntityName = GetCRMData<T>().name,
                        ColumnSet = new ColumnSet(GetPropertiesToReturn<T>())
                    };

                    FilterExpression filter = expression.ToCondition(typeof(T));
                    if (filter != null)
                    {
                        result.Criteria = filter;
                    }

                    var link = expression.GetLinks(typeof(T));
                    if (link != null)
                    {
                        result.LinkEntities.Add(link);
                    }
                }                
            }

            return result;
        }

        private static string[] GetPropertiesToReturn<T>()
        {
            var result = typeof(T).GetPropertiesToReturn();
            return result;
        }

        private static string[] GetPropertiesToReturn(this Type type)
        {
            var result = type.GetProperties()
                .SelectMany(t => t.GetCustomAttributes(typeof(CRMNameAttribute), false).OfType<CRMNameAttribute>().Where(tt => !string.IsNullOrEmpty(tt.Name)))
                .Select(t => t.Name).ToArray();
            return result;
        }

        //private static object ToValue(this MemberExpression member)
        //{
        //    var tt = GetValueExample(member);

        //    if (member.Expression is ConstantExpression &&
        //        member.Member is FieldInfo)
        //    {
        //        object container =
        //            ((ConstantExpression)member.Expression).Value;
        //        object value = ((FieldInfo)member.Member).GetValue(container);
        //        return value;
        //    }
        //    else if (member.Member is PropertyInfo property)
        //    {
        //        object container =
        //            ((ConstantExpression)member.Expression).Value;
        //        object value = property.GetValue(container);
        //        return value;
        //    }
        //    return null;
        //}

        private static object ToValue(this MemberExpression member)
        {
            if (!(member.Expression is MemberExpression || member.Expression is ConstantExpression))
            {
                return null;
            }

            UnaryExpression objectMember = Expression.Convert(member, typeof(object));

            Expression<Func<object>> getterLambda = Expression.Lambda<Func<object>>(objectMember);

            Func<object> getter = getterLambda.Compile();

            object result = getter();

            return result;
        }

        private static LinkEntity GetLinks(this BinaryExpression expression, Type type)
        {
            LinkEntity result = null;

            if (expression != null)
            {

                FilterExpression filter = expression.ToLinkCondition(type);

                LinkEntity linkLeft = null;
                if (expression.Left is BinaryExpression left)
                {
                    linkLeft = left.GetLinks(type);
                }                
                else if (expression.Left is MemberExpression member)
                {
                    var elements = expression.Left.ToString().Split('.');
                    if (elements.Count() == 3)
                    {
                        var principalProperty = elements[1];
                        var secundaryType = member.Expression.Type;
                        linkLeft = ToLinkEntity(principalProperty, secundaryType, type);
                    }
                }

                LinkEntity linkRight = null;
                if (expression.Right is BinaryExpression right)
                {
                    linkRight = right.GetLinks(type);
                }
                else if (expression.Right is MemberExpression member)
                {
                    var elements = expression.Right.ToString().Split('.');
                    if (elements.Count() == 3)
                    {
                        var principalProperty = elements[1];
                        var secundaryType = member.Expression.Type;
                        linkRight = ToLinkEntity(principalProperty, secundaryType, type);
                    }
                }
                result = linkLeft ?? linkRight;
                if (result != null && filter != null)
                {
                    result.LinkCriteria = filter;
                }
            }
            return result;
        }

        private static FilterExpression ToLinkCondition(this BinaryExpression expression, Type type)
        {
            FilterExpression result = null;

            if (expression != null)
            {
                string expLeft = null;
                object valLeft = null;
                FilterExpression conditionsLeft = null;
                if (expression.Left is BinaryExpression left)
                {
                    conditionsLeft = left.ToCondition(type);
                }
                else if (expression.Left is ConstantExpression constant)
                {
                    valLeft = constant.Value;
                }
                else if (expression.Left is MemberExpression member)
                {
                    var elements = expression.Left.ToString().Split('.');

                    valLeft = member.ToValue();

                    if (elements.Count() == 2)
                    {
                        expLeft = elements[1].GetName(type);
                    }
                    else if (elements.Count() == 3)
                    {
                        var principalProperty = elements[1];
                        var secundaryType = member.Expression.Type;
                        expLeft = elements[2].GetName(secundaryType);
                    }
                }

                string expRight = null;
                object valRight = null;
                FilterExpression conditionsRight = null;
                if (expression.Right is BinaryExpression right)
                {
                    conditionsRight = right.ToCondition(type);
                }
                else if (expression.Right is ConstantExpression constant)
                {
                    valRight = constant.Value;
                }
                else if (expression.Right is MemberExpression member)
                {
                    var elements = member.ToString().Split('.');

                    valRight = member.ToValue();

                    if (elements.Count() == 2)
                    {
                        expRight = elements[1].GetName(type);
                    }
                    else if (elements.Count() == 3)
                    {
                        var principalProperty = elements[1];
                        var secundaryType = member.Expression.Type;
                        expRight = elements[2].GetName(secundaryType);
                    }
                }


                var condition = new ConditionExpression();
                if (expLeft != null)
                {
                    condition.AttributeName = expLeft;
                    condition.Values.Add(valRight);
                }
                else if (expRight != null)
                {
                    condition.AttributeName = expRight;
                    condition.Values.Add(valLeft);
                }
                else if (conditionsLeft != null || conditionsRight != null)
                {
                    result = new FilterExpression();
                    if (conditionsLeft != null) result.AddFilter(conditionsLeft);
                    if (conditionsRight != null) result.AddFilter(conditionsRight);
                    var LogOperator = expression.NodeType.ToLogicalOperator();
                    if (LogOperator != null)
                    {
                        result.FilterOperator = LogOperator.Value;
                    }
                }

                if (expLeft != null || expRight != null)
                {
                    result = new FilterExpression();
                    condition.Operator = expression.NodeType.ToConditionOperator();
                    result.Conditions.Add(condition);
                }
            }
            return result;
        }


        private static FilterExpression ToCondition(this BinaryExpression expression, Type type)
        {
            FilterExpression result = null;

            if (expression != null)
            {
                string expLeft = null;
                object valLeft = null;
                FilterExpression conditionsLeft = null;
                if (expression.Left is BinaryExpression left)
                {
                    conditionsLeft = left.ToCondition(type);
                }
                else if (expression.Left is ConstantExpression constant)
                {
                    valLeft = constant.Value;
                }
                else if (expression.Left is MemberExpression member) {
                    var elements = expression.Left.ToString().Split('.');

                    valLeft = member.ToValue();

                    if (elements.Count() == 2)
                    {
                        expLeft = elements[1].GetName(type);
                    }
                }

                string expRight = null;
                object valRight = null;
                FilterExpression conditionsRight = null;
                if (expression.Right is BinaryExpression right)
                {
                    conditionsRight = right.ToCondition(type);
                }
                else if (expression.Right is ConstantExpression constant)
                {
                    valRight = constant.Value;
                }
                else if (expression.Right is MemberExpression member)
                {
                    var elements = member.ToString().Split('.');

                    valRight = member.ToValue();

                    if (elements.Count() == 2)
                    {
                        expRight = elements[1].GetName(type);
                    }
                }


                var condition = new ConditionExpression();
                if (expLeft != null)
                {
                    condition.AttributeName = expLeft;
                    condition.Values.Add(valRight);
                }
                else if (expRight != null)
                {
                    condition.AttributeName = expRight;
                    condition.Values.Add(valLeft);
                }
                else if (conditionsLeft != null || conditionsRight != null)
                {
                    result = new FilterExpression();
                    if (conditionsLeft != null) result.AddFilter(conditionsLeft);
                    if (conditionsRight != null) result.AddFilter(conditionsRight);
                    var LogOperator = expression.NodeType.ToLogicalOperator();
                    if (LogOperator != null) {
                        result.FilterOperator = LogOperator.Value;
                    }
                }

                if (expLeft != null || expRight != null)
                {
                    result = new FilterExpression();
                    condition.Operator = expression.NodeType.ToConditionOperator();
                    result.Conditions.Add(condition);
                }
            }
            return result;
        }

        private static LinkEntity ToLinkEntity(string property, Type typeSecundary, Type typePrincipal)
        {
            var attr = GetCRMAttr(property, typePrincipal);

            (string namePrincipal, _) = GetCRMData(typePrincipal);
            (string nameSecundary, string keySecundary) = GetCRMData(typeSecundary);

            var result = new LinkEntity
            {
                LinkFromEntityName = namePrincipal,
                LinkToEntityName = nameSecundary,
                Columns = new ColumnSet(GetPropertiesToReturn(typeSecundary)),
                LinkFromAttributeName = attr.Name,
                LinkToAttributeName = keySecundary,
                JoinOperator = JoinOperator.Inner
            };
            return result;
        }



        private static LogicalOperator? ToLogicalOperator(this ExpressionType expression)
        {
            switch (expression)
            {
                case ExpressionType.AndAlso:
                    return LogicalOperator.And;
                case ExpressionType.OrElse:
                    return LogicalOperator.Or;
                default:
                    return null;
            }
        }

        public static ConditionOperator ToConditionOperator(this ExpressionType expression)
        {
            switch (expression)
            {
                case ExpressionType.Equal:
                    return ConditionOperator.Equal;
                case ExpressionType.NotEqual:
                    return ConditionOperator.NotEqual;
                case ExpressionType.LessThan:
                    return ConditionOperator.LessThan;
                case ExpressionType.GreaterThan:
                    return ConditionOperator.GreaterThan;
                case ExpressionType.GreaterThanOrEqual:
                    return ConditionOperator.GreaterEqual;
                case ExpressionType.LessThanOrEqual:
                    return ConditionOperator.LessEqual;
                default:
                    return default(ConditionOperator);
            }
        }

    }
}
