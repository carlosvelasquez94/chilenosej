﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.Xrm.Sdk.Query;
using Tanner.CRM.Models;
using Tanner.CRM.Repositories;
using Tanner.CRM.Services.Interfaces;
using Tanner.CRM.Utils;

namespace Tanner.CRM.Services
{
    public class ServiceBase<T> : IServiceBase<T> where T : IEntityBase
    {
        protected readonly IRepositoryBase<T> _repository;

        public ServiceBase(IRepositoryBase<T> repository)
        {
            _repository = repository;
        }

        public bool Any(Expression<Func<T, bool>> predicate)
        {
            bool result = _repository.Any(predicate);
            return result;
        }

        public T FirstOrDefault(Expression<Func<T, bool>> predicate)
        {
            T result = _repository.FirstOrDefault(predicate);
            return result;
        }

        public (IEnumerable<T>, int) GetAllBy(Expression<Func<T, bool>> predicate, int start = 1, int count = 100)
        {
            (IEnumerable<T>, int) result = _repository.GetAllBy(predicate, start, count);
            return result;
        }

        public T GetByID(Guid id)
        {
            T result = _repository.GetByID(id);
            return result;
        }

        public T UpSert(T entity)
        {
            T result = _repository.UpSert(entity);
            return result;
        }
    }
}
