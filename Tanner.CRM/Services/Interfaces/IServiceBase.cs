﻿
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Tanner.CRM.Models;

namespace Tanner.CRM.Services.Interfaces
{
    public interface IServiceBase<T> where T : IEntityBase
    {
        T GetByID(Guid id);

        T FirstOrDefault(Expression<Func<T, bool>> predicate);

        bool Any(Expression<Func<T, bool>> predicate);

        (IEnumerable<T>, int) GetAllBy(Expression<Func<T, bool>> predicate, int start = 1, int count = 100);

        T UpSert(T entity);
    }
}
