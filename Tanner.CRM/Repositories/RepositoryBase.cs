﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Tanner.CRM.Models;
using Tanner.CRM.Utils;

namespace Tanner.CRM.Repositories
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : IEntityBase
    {
        private readonly IOrganizationService _orgService;

        public RepositoryBase(IOrganizationService orgService)
        {
            _orgService = orgService;
        }


        public T FirstOrDefault(QueryExpression query)
        {
            query.PageInfo = new PagingInfo
            {
                Count = 1,
                PageNumber = 1,
                ReturnTotalRecordCount = false
            };

            (IEnumerable<T> mapping, _) = GetAllBy(query);
            T result = mapping.FirstOrDefault();
            return result;
        }
        
        public T FirstOrDefault(Expression<Func<T, bool>> predicate)
        {
            QueryExpression query = predicate.ToExpression();
            T result = FirstOrDefault(query);
            return result;
        }


        public (IEnumerable<T>, int) GetAllBy(QueryExpression query)
        {
            EntityCollection response = _orgService.RetrieveMultiple(query);
            IEnumerable<T> mapping = response.Entities.Select(t => t.ToEntityBase<T>()).ToList();
            (IEnumerable<T>, int) result = (mapping, response.TotalRecordCount);
            return result;
        }

        public (IEnumerable<T>, int) GetAllBy(Expression<Func<T, bool>> predicate, int start = 1, int count = 100)
        {
            QueryExpression query = predicate.ToExpression();

            query.PageInfo = new PagingInfo
            {
                Count = count,
                PageNumber = start,
                ReturnTotalRecordCount = true
            };

            return GetAllBy(query);
        }

        public IEnumerable<T> GetAllBy(Expression<Func<T, bool>> predicate)
        {
            QueryExpression query = predicate.ToExpression();
            (IEnumerable<T> result, _)  = GetAllBy(query);
            return result;
        }


        public T GetByID(Guid id)
        {
            T result = FirstOrDefault(t => t.ID == id);
            return result;
        }



        public T UpSert(T entity, Guid? id = null)
        {
            bool isNew = id == null || id == Guid.Empty;
            entity.ID = isNew ? Guid.NewGuid() : id.Value;
            Entity crmEntity = entity.ToEntity();

            if (isNew)
            {
                _orgService.Create(crmEntity);
            }
            else
            {
                _orgService.Update(crmEntity);
            }
            
            return entity;
        }


        public bool Any(QueryExpression query)
        {
            query.PageInfo = new PagingInfo
            {
                Count = 0,
                PageNumber = 1,
                ReturnTotalRecordCount = true
            };

            (_, int totalRecord) = GetAllBy(query);
            bool result = totalRecord > 0;
            return result;
        }

        public bool Any(Expression<Func<T, bool>> predicate)
        {
            QueryExpression query = predicate.ToExpression();
            bool result = Any(query);
            return result;
        }
    }
}
