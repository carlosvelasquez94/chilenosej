﻿using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Tanner.CRM.Models;

namespace Tanner.CRM.Repositories
{
    public interface IRepositoryBase<T> where T: IEntityBase
    {
        T GetByID(Guid id);
        
        T FirstOrDefault(QueryExpression query);

        bool Any(Expression<Func<T, bool>> predicate);

        bool Any(QueryExpression query);

        (IEnumerable<T>, int) GetAllBy(QueryExpression query);


        (IEnumerable<T>, int) GetAllBy(Expression<Func<T, bool>> predicate, int start, int count);

        IEnumerable<T> GetAllBy(Expression<Func<T, bool>> predicate);

        T FirstOrDefault(Expression<Func<T, bool>> predicate);

        T UpSert(T entity, Guid? id = null);
    }
}
