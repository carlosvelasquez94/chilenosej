﻿
using System;

namespace Tanner.CRM.Models
{
    public interface IEntityBase
    {
        Guid ID { get; set; }
    }
}
