﻿using Microsoft.Extensions.Options;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.Net;
using System.ServiceModel.Description;

namespace Tanner.CRM.Configurations
{
    public interface ICRMOrganizationResolve
    {
        IOrganizationService GetOrgService();
    }

    public class CRMOrganizationResolve: ICRMOrganizationResolve
    {
        private readonly CRMConfiguration _configuration;

        public CRMOrganizationResolve(IOptions<CRMConfiguration> conf)
        {
            _configuration = conf.Value;
        }

        public IOrganizationService GetOrgService()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            IServiceManagement<IOrganizationService> orgServiceManagement =
                         ServiceConfigurationFactory.CreateManagement<IOrganizationService>(
                         new Uri(_configuration.Uri));

            AuthenticationProviderType endpointType = orgServiceManagement.AuthenticationType;

            // Set the credentials
            AuthenticationCredentials credentials = _configuration.GetCredentials(orgServiceManagement, endpointType);

            // Get the organization service proxy
            IOrganizationService OrgService = GetProxy<IOrganizationService, OrganizationServiceProxy>(orgServiceManagement, credentials);

            // Early binding
            var osp = OrgService as OrganizationServiceProxy;
            osp.ServiceConfiguration.CurrentServiceEndpoint.Behaviors.Add(new ProxyTypesBehavior());
            return osp;
        }

        private static TProxy GetProxy<TService, TProxy>(IServiceManagement<TService> serviceManagement, AuthenticationCredentials authCredentials)
            where TService : class
            where TProxy : ServiceProxy<TService>
        {
            Type classType = typeof(TProxy);

            if (serviceManagement.AuthenticationType !=
                AuthenticationProviderType.ActiveDirectory)
            {
                AuthenticationCredentials tokenCredentials = serviceManagement.Authenticate(authCredentials);

                // Obtain discovery/organization service proxy for Online environments.
                // Instantiate a new class of type using the 2 parameter constructor of type IServiceManagement and SecurityTokenResponse.
                return (TProxy)classType
                    .GetConstructor(new Type[] { typeof(IServiceManagement<TService>), typeof(SecurityTokenResponse) })
                    .Invoke(new object[] { serviceManagement, tokenCredentials.SecurityTokenResponse });
            }

            // Obtain Organization service proxy for ActiveDirectory environment.
            // Instantiate a new class of type using the 2 parameter constructor of type IServiceManagement and ClientCredentials.
            return (TProxy)classType
                .GetConstructor(new Type[] { typeof(IServiceManagement<TService>), typeof(ClientCredentials) })
                .Invoke(new object[] { serviceManagement, authCredentials.ClientCredentials });
        }
    }
}
