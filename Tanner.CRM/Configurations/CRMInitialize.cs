﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.Net;
using System.ServiceModel.Description;
using Tanner.CRM.Repositories;
using Tanner.CRM.Services;
using Tanner.CRM.Services.Interfaces;

namespace Tanner.CRM.Configurations
{
    public static class CRMInitialize
    {
        public static void AddTannerCRM(this IServiceCollection services, Action<CRMConfiguration> option)
        {
            services.Configure(option);
            Common(services);
        }

        public static void AddTannerCRM(this IServiceCollection services, IConfiguration configuration)
        {
            IConfiguration option = configuration.GetSection(nameof(CRMConfiguration));
            services.Configure<CRMConfiguration>(option);
            Common(services);
        }

        private static void Common(IServiceCollection services)
        {
            services.AddScoped(typeof(IServiceBase<>), typeof(ServiceBase<>));
            services.AddScoped(typeof(IRepositoryBase<>), typeof(RepositoryBase<>));
            
            services.AddScoped<ICRMOrganizationResolve, CRMOrganizationResolve>();
            services.AddScoped<IOrganizationService>((provider)=> ResolveIOrganizationService(provider));
        }

        private static IOrganizationService ResolveIOrganizationService(IServiceProvider provider)
        {
            var resolve = provider.GetRequiredService<ICRMOrganizationResolve>();
            var result = resolve.GetOrgService();
            return result;
        }


    }
}
