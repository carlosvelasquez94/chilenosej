# Tanner.CRM

## Release 1.0.4.0 - 2018-08-26
* Actualizando repositorios de CRM que permite la búsqueda con expresiones lambda.

## Release 1.0.3.0 - 2018-08-23
* Se agrega funcionalidad para saber si existe un elemento en CRM que cumpla cierta condiciones.

## Release 1.0.2.0 - 2018-08-19
* Implementando funcionalidad FirstOrDefault para repositorio base.

## Release 1.0.1.0 - 2018-08-08
* Agregando método UpSert que permite la inserción o actualización de una entidad.

## Release 1.0.0.0 - 2018-06-24
* Versión inicial