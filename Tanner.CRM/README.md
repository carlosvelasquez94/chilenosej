# Tanner.CRM

[[CHANGE]]


## Instalación para proyectos Core

##### ¿Cómo instalo el paquete?

```
nuget install [[CHANGE]]
```

```
Install-Package [[CHANGE]]
```

### Uso 

##### ¿Cómo usar [[CHANGE]] para [[CHANGE]]?

1. Instalar el paquete en [[CHANGE]].
2. En el Startup.cs en el método "ConfigureServices" llamar la extensión [[CHANGE]] definida para "IServiceCollection" pasando lo parámetros requeridos

 ```csharp
 ```
 
Donde:


 ```csharp

 ``` 
 
###  Enlaces de interés

[Documentación Injección de Dependencia en ASP.Net Core](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-2.1)

[Documentación Azure Function en ASP.Net Core] (https://docs.microsoft.com/es-es/azure/azure-functions/)